import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: <String>[
    'email',
    'https://www.googleapis.com/auth/spreadsheets.readonly'
  ]
);

void main() {
  runApp(
    MaterialApp(
      title: '5LOP Awards',
      home: HomePage()
    )
  );
}

class HomePage extends StatefulWidget {
  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  GoogleSignInAccount _currentUser;

  @override
  void initState() {
    super.initState();
    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) {
      setState(() {
        _currentUser = account;
      });
      if (_currentUser != null) {
        print('Successfully Logged In!');
      }
    });
  }

  Future<void> _handleSignIn() async {
    try {
      await _googleSignIn.signIn();
    } catch (error) {
      print(error);
    }
  }

  Future<void> _handleSignOut() async {
    _googleSignIn.disconnect();
  }

  List<Widget> _buildBody() {
    if (_currentUser != null) {
      return <Widget>[
        RaisedButton(
          child: const Text("Sign Out"),
          onPressed: _handleSignOut,
        )
      ];
    } else {
      return <Widget>[
        RaisedButton(
          child: const Text("Sign In with Google"),
          onPressed: _handleSignIn,
        )
      ];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("5LOP Awards"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: _buildBody(),
          ),
        ));
  }
}
